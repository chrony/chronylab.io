#!/bin/sh

set -e

cd public/releases

out=index.html

rm -f "$out"

cat > "$out" <<EOF
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Index of /releases</title>
</head>
<body>
<h2>Index of /releases</h2>
<table>
EOF
for i in *; do
	[ "$i" = "$out" ] && continue
	stat -c '<tr><td><a href="%n">%n</a></td></tr>' "$i"
done >> index.html

cat >> "$out" <<EOF
</table>
</body>
</html>
EOF
