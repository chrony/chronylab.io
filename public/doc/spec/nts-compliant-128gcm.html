<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=0.5">
<meta name="generator" content="Asciidoctor 2.0.20">
<title>chrony – Compliant NTS key exporter context for AES-128-GCM-SIV AEAD</title>
<link rel="stylesheet" href="./../../css/asciidoc.css" type="text/css" />
<link rel="stylesheet" href="./../../css/chrony.css" type="text/css" />
</head>
<body class="article">
<div id="menu">
  <h1>c h r o n y</h1>
  <ul>
    <li><a href="./../../index.html">Introduction</a></li>
    <li><a href="./../../news.html">News</a></li>
    <li><a href="./../../download.html">Download</a></li>
    <li><a href="./../../documentation.html">Documentation</a></li>
    <li><a href="./../../faq.html">FAQ</a></li>
    <li><a href="./../../examples.html">Examples</a></li>
    <li><a href="./../../lists.html">Mailing lists</a></li>
    <li><a href="./../../comparison.html">Comparison</a></li>
    <li><a href="./../../contributing.html">Contributing</a></li>
    <li><a href="./../../links.html">Links</a></li>
  </ul>
</div>
<div id="document">
<div id="header">
<h1>Compliant NTS key exporter context for AES-128-GCM-SIV AEAD</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>This document specifies an NTS-KE record for negotiation of an
<a href="https://datatracker.ietf.org/doc/html/rfc8915">RFC-8915</a>-compliant NTS key
exporter context for the AES-128-GCM-SIV AEAD.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_background">Background</h2>
<div class="sectionbody">
<div class="paragraph">
<p>In <code>chrony</code> version 4.0 was added support for the Network Time Security (NTS)
authentication mechanism. The AEAD algorithm used by NTS for encryption and
authentication of NTP messages is negotiated between the client and server in
NTS-KE. The only AEAD that <code>chrony</code> initially supported was AES-SIV-CMAC-256.</p>
</div>
<div class="paragraph">
<p>In version 4.4 was added support for the AES-128-GCM-SIV AEAD. The main
advantage of this AEAD are shorter keys, which makes the NTS cookies and
NTS-protected messages shorter. Delivery of shorter NTP messages is more
reliable over Internet, where some major network operators are known to block
or limit rate of longer NTP packets as a mitigation against NTP mode-6 and
mode-7 amplification attacks.</p>
</div>
<div class="paragraph">
<p>The AES-128-GCM-SIV support has a bug. The second two octets of the NTS key
exporter context do not follow the
<a href="https://datatracker.ietf.org/doc/html/rfc8915#section-5.1">section 5.1</a> of
RFC 8915. The value that is included in the context is 15 (AES-SIV-CMAC-256)
instead of 30 (AES-128-GCM-SIV). This bug prevents interoperation with NTS
implementations correctly following RFC 8915, in both server-client and
client-server directions.</p>
</div>
<div class="paragraph">
<p>When this document was written, <code>chrony</code> was the only known implementation
supporting AES-128-GCM-SIV. The bug can easily be fixed, but all clients and
servers would need be updated at the same time to avoid disruptions.</p>
</div>
<div class="paragraph">
<p>Instead, a new NTS-KE record is introduced to enable a gradual migration to the
compliant context. The expectation is that <code>chrony</code> and other implementations
that want to interoperate with <code>chrony</code> will default to the noncompliant
context until all servers and clients can negotiate the compliant context with
the new NTS-KE record. At that point, the default can be switched to the
compliant context. When everything defaults to the compliant context, the
NTS-KE record can be deprecated.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_compliant_aes_128_gcm_siv_exporter_context">Compliant AES-128-GCM-SIV Exporter Context</h2>
<div class="sectionbody">
<div class="paragraph">
<p>The Compliant AES-128-GCM-SIV Exporter Context NTS-KE record negotiates the use
of the compliant exporter context for the AES-128-GCM-SIV AEAD. The Record Type
number is 1024. The body MUST be empty. The critical bit MUST NOT be set for
this record.</p>
</div>
<div class="paragraph">
<p>An NTS client which supports the Compliant AES-128-GCM-SIV Exporter Context
record SHOULD add it to every NTS-KE request which includes AES-128-GCM-SIV in
the AEAD Algorithm Negotiation NTS-KE record.</p>
</div>
<div class="paragraph">
<p>If a server which supports the Compliant AES-128-GCM-SIV Exporter Context
record receives a request containing this record and selects the
AES-128-GCM-SIV AEAD, it MUST include this record in its response and use the
compliant exporter context to generate the NTS keys used for this client. It
the server selects a different AEAD algorithm, it MUST NOT include this record
in the NTS-KE response.</p>
</div>
<div class="paragraph">
<p>If the client receives an NTS-KE response containing this record, it MUST use
the compliant exporter context to generate the NTS keys.</p>
</div>
<div class="paragraph">
<p>If the NTS-KE request or response does not contain this record, the server and
client SHOULD use the noncompliant exporter context corresponding to the
AES-SIV-CMAC-256 AEAD until all servers and clients in use negotiate the
compliant context with this record.</p>
</div>
<div class="paragraph">
<p>The client MAY switch between the two exporter contexts if all responses
received from the server after an NTS-KE session are NTS NAK in order to
interoperate with servers that use the compliant context, but do not support
this NTS-KE record. The server MUST NOT provide in one NTS-KE response a set of
cookies mixing the two exporter contexts to not interfere with the client-side
switching of the context. The server MAY include both sets of keys using the
different contexts in one cookie in order to detect which context is used by
the client and send a response which will pass the client&#8217;s authentication
check.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_acknowledgements">Acknowledgements</h2>
<div class="sectionbody">
<div class="paragraph">
<p>This workaround for the <code>chrony</code> bug was suggested by Daniel Franke.</p>
</div>
</div>
</div>
</div>
<div id="footer">
<div id="footer-text">
Last updated 2024-09-24 15:40:32 +0200
</div>
</div>
</body>
</html>
